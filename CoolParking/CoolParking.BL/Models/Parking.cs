﻿// TODO: implement class Parking.
//       Implementation details are up to you, they just have to meet the requirements 
//       of the home task and be consistent with other classes and tests.
using System.Collections.Generic;
using System.Text;

namespace CoolParking.BL.Models
{
    public sealed class Parking
    {
        private static Parking instance;
        private Parking()
        {
            this.Balance = Settings.balance;
            this.Vehicles = new List<Vehicle>(Settings.countOfPlaces);
        }
        public decimal Balance { get; set; }
        public List<Vehicle> Vehicles{get;set;}
        public static Parking Instance
        {
            get
            {
                if (instance == null)
                    instance = new Parking();
                return instance;
            }
        }
         public void AddVehicle(Vehicle vehicle)
         {
             Vehicles.Add(vehicle);
         }
        public void RemoveVehicle(Vehicle vehicle)
        {
            Vehicles.Remove(vehicle);
        }
        public bool IsSameId(string id)
        {
            bool check = false;
            foreach (var vehicle in Vehicles)
                if (id == vehicle.Id)
                {
                    check = true;
                }
            return check;
        }
    }
}
