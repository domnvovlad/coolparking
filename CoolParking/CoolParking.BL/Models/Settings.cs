﻿// TODO: implement class Settings.
//       Implementation details are up to you, they just have to meet the requirements of the home task.
using System;
namespace CoolParking.BL.Models
{
    static class Settings
    {
        public static decimal balance = 0;
        public static int countOfPlaces = 10;
        public static double interval = 5000;
        public static double period = 60000;
        public static double fineCoefficient = 2.5;
        public static double Tarrif(VehicleType vehicleType)
        {
            switch(vehicleType)
            {
                case VehicleType.PassengerCar:
                    {
                        return 2;
                    }
                case VehicleType.Truck:
                    {
                        return 5;
                    }
                case VehicleType.Bus:
                    {
                        return 3.5;
                    }
                case VehicleType.Motorcycle:
                    {
                        return 5;
                    }
                default:
                    throw new InvalidOperationException();
            }
        }
    }
}