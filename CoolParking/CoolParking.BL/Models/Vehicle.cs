﻿// TODO: implement class Vehicle.
//       Properties: Id (string), VehicleType (VehicleType), Balance (decimal).
//       The format of the identifier is explained in the description of the home task.
//       Id and VehicleType should not be able for changing.
//       The Balance should be able to change only in the CoolParking.BL project.
//       The type of constructor is shown in the tests and the constructor should have a validation, which also is clear from the tests.
//       Static method GenerateRandomRegistrationPlateNumber should return a randomly generated unique identifier.

using System;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Text.RegularExpressions;

namespace CoolParking.BL.Models
{
    public class Vehicle
    {
        private string id;
        private decimal balance;
        Regex regex= new Regex(@"[A-Z]{2}-\d{4}-[A-Z]{2}$");
        public Vehicle(string id, VehicleType vehicleType, decimal balance)
        {
            this.Id = id;
            this.VehicleType = vehicleType;
            this.Balance = balance;
        }
        public string Id { private set
            {
                if (regex.IsMatch(value)) 
                    id = value;
                else throw new ArgumentException();
            }
            get { return id; }
        }
        public VehicleType VehicleType { get; private set; }
        public decimal Balance
        {
            internal set
            {
                if (value < 0) throw new ArgumentException();
                else balance = value;
            }
            get { return balance; }
        }
        public static string GenerateRandomRegistrationPlateNumber()
        {
            string id = RandomTwoSymvols() + "-" + RandomFourNumbers()+"-"+RandomTwoSymvols();
            foreach (var pair in Parking.Instance.Vehicles)
                if (id == pair.Id) GenerateRandomRegistrationPlateNumber();
            return id;
        }
        private static string RandomTwoSymvols()
        {
            string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            string letters="";
            for (int i = 0; i < 2; i++)
            {
                Random rand = new Random();
                int num = rand.Next(0, chars.Length - 1);
                letters += chars[num];
            }
            return letters;
        }
        private static string RandomFourNumbers()
        {
            string letters = "";
            for (int i = 0; i < 4; i++)
            {
                Random rand = new Random();
                int num = rand.Next(0, 9);
                letters += num.ToString();
            }
            return letters;
        }
    }
}
