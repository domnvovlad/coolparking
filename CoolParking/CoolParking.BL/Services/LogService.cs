﻿// TODO: implement the LogService class from the ILogService interface.
//       One explicit requirement - for the read method, if the file is not found, an InvalidOperationException should be thrown
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in LogServiceTests you can find the necessary constructor format.
using System;
using System.IO;
using CoolParking.BL.Interfaces;

namespace CoolParking.BL.Services
{
    public class LogService : ILogService
    {
        public LogService(string _logFIlePath)
        {
            this.LogPath = _logFIlePath;
        }
        public string LogPath { get; set; }

        public string Read()
        {
            try
            {
                using(StreamReader reader = new StreamReader(LogPath) )
                {
                    return reader.ReadToEnd();
                }
            }
            catch(Exception)
            {
                throw new InvalidOperationException();
            }
        }

        public void Write(string logInfo)
        {
            using (var file = new StreamWriter(LogPath, true))
            {
                file.WriteLine(logInfo);
            }
        }
    }
}