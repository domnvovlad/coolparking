﻿// TODO: implement the ParkingService class from the IParkingService interface.
//       For try to add a vehicle on full parking InvalidOperationException should be thrown.
//       For try to remove vehicle with a negative balance (debt) InvalidOperationException should be thrown.
//       Other validation rules and constructor format went from tests.
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in ParkingServiceTests you can find the necessary constructor format and validation rules.
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;

namespace CoolParking.BL.Services
{
    public class ParkingService : IParkingService
    {
        public ParkingService(ITimerService _withdrawTimer, ITimerService _logTimer, ILogService _logService)
        {
            Parking = Parking.Instance;
        }
        public Parking Parking { get; set; }
        public void AddVehicle(Vehicle vehicle)
        {
            if (GetFreePlaces() == 0|| Parking.IsSameId(vehicle.Id)) throw new ArgumentException();
            else Parking.AddVehicle(vehicle);
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }

        public decimal GetBalance()
        {
            return Parking.Balance;
        }

        public int GetCapacity()
        {
            return Parking.Vehicles.Capacity;
        }

        public int GetFreePlaces()
        {
            return Parking.Vehicles.Capacity - Parking.Vehicles.Count;
        }

        public TransactionInfo[] GetLastParkingTransactions()
        {
            throw new NotImplementedException();
        }

        public ReadOnlyCollection<Vehicle> GetVehicles()
        {
            return new ReadOnlyCollection<Vehicle>(Parking.Vehicles);
        }

        public string ReadFromLog()
        {
            throw new NotImplementedException();
        }

        public void RemoveVehicle(string vehicleId)
        {
            bool exist = false;
            foreach(var pair in Parking.Vehicles)
                if (vehicleId == pair.Id &&
                    pair.Balance.CompareTo(Settings.Tarrif(pair.VehicleType)) > 0)
                {
                    Parking.RemoveVehicle(pair);
                    exist = true;
                }
             if(!exist) throw new ArgumentException();
        }

        public void TopUpVehicle(string vehicleId, decimal sum)
        {
            if (sum < 0) throw new ArgumentException();
            else
            {
                bool exist = false;
                foreach (var vehicle in Parking.Vehicles)
                    if (vehicleId == vehicle.Id)
                    {
                        vehicle.Balance += sum;
                        exist = true;
                    }
                if (!exist) throw new ArgumentException();
            }
        }
        private static void PassMoney()
        {

        }

    }
}