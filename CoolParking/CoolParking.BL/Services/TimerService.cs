﻿// TODO: implement class TimerService from the ITimerService interface.
//       Service have to be just wrapper on System Timers.
using System.Timers;
using CoolParking.BL.Interfaces;

namespace CoolParking.BL.Services
{
    public class TimerService : ITimerService
    {
        private static Timer timer;
        public double Interval { get ; set ; }

        public event ElapsedEventHandler Elapsed;

        public void Dispose()
        {
            timer.Dispose();
        }

        public void Start()
        {
            timer = new Timer(Interval);
            timer.Elapsed += Elapsed;
            timer.AutoReset = true;
            timer.Enabled = true;
        }

        public void Stop()
        {
            timer.Enabled = false;
            timer.Stop();
        }
    }
}